# Lint helpers


1. Copy the three scripts to ~/bin/ and set execute perms.

2. From the root dir of a repo with phpcs installed:

	- `lintfullstop <file>.php` will apply comment full-stop lints identified by `composer lint`.

	- `lintcamelsnake <file>.php` will apply camelCase/snake_case lints for variable names. Methods/properties/functions are intentionally excluded.

	- `linthelper` will run `lintfullstop` and `lintcamelsnake` on all files identified by `composer lint`.
